#include <stdio.h>
#include <stdlib.h>
#include <zlog.h>

#include "graver/configuration/version.h"

int main() {
    int rc;
    rc = dzlog_init("../conf/zlog.conf", "my_cat");

    if (rc) {
        printf("init zlog failed! rc:%d\n", rc);
        zlog_fini();
        exit(EXIT_FAILURE);
    }

    // TODO xxx
    // BUG xxx
    // FIXME xxx
    // NOTE xxx
    // TAG xxx

    dzlog_info("graver major version is:%d", graver_get_version_major());
    dzlog_info("graver minor version is:%d", graver_get_version_minor());
    dzlog_info("graver patch version is:%d", graver_get_version_patch());
    dzlog_info("graver patch version is:%d", graver_get_version_patch());

    zlog_fini();

    return 0;
}

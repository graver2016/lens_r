#include "graver/configuration/version.h"

#include <stdlib.h>

#include "graver/configuration/configuration.h"

int graver_get_version_major() {
    return (int)strtol(GRAVER_VERSION_STR2(GRAVER_VERSION_MAJOR), NULL, 10);
}
int graver_get_version_minor() {
    return (int)strtol(GRAVER_VERSION_STR2(GRAVER_VERSION_MINOR), NULL, 10);
}
int graver_get_version_patch() {
    return (int)strtol(GRAVER_VERSION_STR2(GRAVER_VERSION_PATCH), NULL, 10);
}
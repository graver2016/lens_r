#!/bin/bash

PRESET=gnu-debug
rm -rf build/${PRESET}
cmake --preset=${PRESET}
cmake --build --preset=${PRESET}
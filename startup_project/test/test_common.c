#include "test_common.h"

#include <stdio.h>
#include <zlog.h>

#include "graver/configuration/version.h"

void test_common_one() {
    printf("\n");
    dzlog_info("test_common_one start");

    dzlog_info("graver major version is:%d", graver_get_version_major());
    dzlog_info("graver minor version is:%d", graver_get_version_minor());
    dzlog_info("graver patch version is:%d", graver_get_version_patch());

    dzlog_info("test_common_one end");
}

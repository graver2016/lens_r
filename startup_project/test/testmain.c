#include <CUnit/Basic.h>
#include <CUnit/CUnit.h>
#include <CUnit/Console.h>
#include <CUnit/TestDB.h>
#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <zlog.h>

#include "test_common.h"

// suite 初始化
static int suite_success_init() {
    return 0;
}

// suite 清理
static int suite_success_clean() {
    return 0;
}

static CU_TestInfo test_configuration_case[] = {{"test_common_one:", test_common_one}, CU_TEST_INFO_NULL};

// 测试套件
static CU_SuiteInfo      suites[] = {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-field-initializers"
    {"test_common", suite_success_init, suite_success_clean, NULL, NULL, test_configuration_case}, CU_TEST_INFO_NULL};
#pragma clang diagnostic pop

int main(/*int argc, char** argv*/) {
    int rc;
    rc = dzlog_init("zlog.conf", "my_cat");

    if (rc) {
        printf("init zlog failed! rc:%d\n", rc);
        zlog_fini();
        exit(EXIT_FAILURE);
    }

    dzlog_info("graver test start...");

    // test main
    if (CU_initialize_registry()) {
        fprintf(stderr, " Initialization of Test Registry failed.");
        exit(EXIT_FAILURE);
    }
    assert(NULL != CU_get_registry());
    assert(!CU_is_test_running());

    if (CUE_SUCCESS != CU_register_suites(suites)) {
        exit(EXIT_FAILURE);
    }

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();

    CU_cleanup_registry();

    dzlog_info("graver test end...");
    zlog_fini();

    return (int)(CU_get_error());
}

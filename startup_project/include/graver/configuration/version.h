#pragma once

#include "graver/configuration/configuration.h"

#define GRAVER_VERSION_MAJOR_COPY (GRAVER_VERSION_MAJOR)  // NOLINT
#define GRAVER_VERSION_MINOR_COPY (GRAVER_VERSION_MINOR)  // NOLINT
#define GRAVER_VERSION_PATCH_COPY (GRAVER_VERSION_PATCH)  // NOLINT

#define GRAVER_VERSION_STR(R) #R
#define GRAVER_VERSION_STR2(R) GRAVER_VERSION_STR(R)

int graver_get_version_major();
int graver_get_version_minor();
int graver_get_version_patch();

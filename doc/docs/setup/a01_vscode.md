## 下载

下载地址: [https://code.visualstudio.com/#alt-downloads](https://code.visualstudio.com/#alt-downloads)

!!! note "注意"
    推荐下载压缩包
    
![](/assets/image/setup/a01_vscode/001.png)

## 启动配置
!!! note "注意"
    下载 `vscode` 之后,不要着急启动,应该先配置 `desktop`

    关于启动参数和 `Portable Mode` 的参考文档:

    启动参数: [https://code.visualstudio.com/docs/editor/command-line](https://code.visualstudio.com/docs/editor/command-line)
    
    `Portable Mode`: [https://code.visualstudio.com/docs/editor/portable](https://code.visualstudio.com/docs/editor/portable)

### 下载一个 logo

!!! bug
    ubuntu 修改图标不成功,暂时不纠结原因,当前的配置仅仅是为了启动时能快速的分辨加载的是哪个用户目录    

比如可以从 [https://www.iconfont.cn/collections/detail?spm=a313x.user_detail.i1.dc64b3430.5cf63a81xYAiId&cid=46449](https://www.iconfont.cn/collections/detail?spm=a313x.user_detail.i1.dc64b3430.5cf63a81xYAiId&cid=46449) 下载一个 `c++` 图标, 大小选择 128 即可

![](/assets/image/setup/a01_vscode/002.png)

### desktop 配置
将下载的压缩包复制到一个目录,并按照如下目录结构设置
```shell
.
├── code # vscode 目录 将来升级 vscode 直接将此目录替换即可
│   ├── bin
│   │   ├── code
│   │   └── code-tunnel
├── code-stable-x64-1707853307.tar.gz # 下载的 vscode 压缩包
└── cpp # c++ 目录,启动时指定 --user-data-dir 为此目录
│   ├── data
│   └── logo.png
└── java # 比如再添加一个 java 目录用来开发 java
    ├── data
    └── logo.png
     
449 directories, 993 files
```

在 `/.local/share/applications/code-cpp.desktop`中添加如下内容
```shell
[Desktop Entry]
Name=code-cpp
Comment=code cpp
Exec=/home/laolang/program/vscode/multi_code/code/bin/code --user-data-dir /home/laolang/program/vscode/multi_code/cpp/data
Icon=/home/laolang/program/vscode/multi_code/cpp/logo.png
Terminal=false
StartupNotify=true
Type=Application
```

启动后在 `data` 目录能看到内容说明配置成功

## 安装基本需要的插件
### 插件列表  

```json5
{
    "recommendations": [
        // 汉化
        "MS-CEINTL.vscode-language-pack-zh-hans",
        // clang-format 格式化
        "xaver.clang-format",
        // clangd 代码提示
        "llvm-vs-code-extensions.vscode-clangd",
        // cmake 代替着色与代码提示支持
        "josetr.cmake-language-support-vscode",
        // lldb 调试
        "vadimcn.vscode-lldb",
        // 静态检查
        "CS128.cs128-clang-tidy",
        // 快速生成 doxygen 模板
        "cschlosser.doxdocgen",
        // 将错误显示在当前行
        "usernamehw.errorlens",
        // 必备 git 插件，可以查看 git 分支图
        "mhutchie.git-graph",
        // git 提交插件
        "redjue.git-commit-plugin",
        // 文件图标
        "vscode-icons-team.vscode-icons",
        // 将某些 tasks 放到状态栏
        "spencerwmiles.vscode-task-buttons",
        // 浏览并执行当前项目的 tasks
        "spmeesseman.vscode-taskexplorer",
        // todo 功能
        "Gruntfuggly.todo-tree",
        "wayou.vscode-todo-highlight"
    ]
}
```

### 如何安装
随便打开一个目录,并新建 `.vscode/extensions.json`,打开扩展市场并筛选

![](/assets/image/setup/a01_vscode/003.png)

## 示例工程
### 地址
[https://gitlab.com/graver2016/lens_r/-/tree/main/startup_project](https://gitlab.com/graver2016/lens_r/-/tree/main/startup_project)

### 使用前准备
**需要安装的软件**

- cmake
- clangd
- clang-tidy
- lldb
- ninja
- python3
    - virtualenv
- doxygen
- graphviz
- CUnit
- zlog

#### cunit 指南

##### 官网

github: [https://github.com/jacklicn/CUnit](https://github.com/jacklicn/CUnit)

官方手册: [https://cunit.sourceforge.net/doc/index.html](https://cunit.sourceforge.net/doc/index.html)

##### 一些教程

1. [【单元测试】CUnit 用户手册（中文）](https://blog.csdn.net/benkaoya/article/details/95887879)
2. [【单元测试】CUnit 单元测试框架（不支持 mock 功能）](https://blog.csdn.net/benkaoya/article/details/95870801)
3. [c 语言单元测试框架--CuTest](https://www.cnblogs.com/pingwen/p/9216004.html)

##### 关于安装

[ubuntu 下安装 CUnit 出现的问题及解决](https://www.codenong.com/cs106365824)

安装过程如下

```shell
sudo apt-get update
sudo apt-get install build-essential automake autoconf libtool
mv configure.in configure.ac
aclocal
autoconf
autoheader
libtoolize --automake --copy --debug --force
automake --add-missing
automake
./configure
make
sudo make install
```

#### zlog 指南
github: [https://github.com/HardySimpson/zlog](https://github.com/HardySimpson/zlog)

中文文档: [http://hardysimpson.github.io/zlog/UsersGuide-CN.html](http://hardysimpson.github.io/zlog/UsersGuide-CN.html)

!!! note "注意"
    1. 安装 `zlog` 后, 需要执行 `sudo ldconfig` , [参考手册 3.1 编译和安装 zlog](http://hardysimpson.github.io/zlog/UsersGuide-CN.html#htoc6)
    2. `zlog` 要求日志目录提前存在, 所以如果 `zlog` 一直初始化失败, 先检查一下日志目录是否存在


### 测试能否运行
1. 进入项目根目录,并执行以下命令
```shell
virtualenv .venv # 创建 python 环境
source .venv/bin/activate # 选择 python 环境
pip3 install -r requirements.txt # 安装 python 依赖
./run.sh # shell 脚本运行
./cm.py rr # python 脚本运行
```

一个运行实例如下
```shell
(.venv) 06:08:16 laolang:startup_project(main)$ ./run.sh 
Preset CMake variables:

  CMAKE_BUILD_TYPE="Release"
  CMAKE_C_COMPILER="/usr/bin/clang"
  CMAKE_C_FLAGS="-Wall -Wextra"

-- The C compiler identification is Clang 14.0.0
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working C compiler: /usr/bin/clang - skipped
-- Detecting C compile features
-- Detecting C compile features - done
-- ENABLE_COVERAGE:OFF
-- Configuring done (0.1s)
-- Generating done (0.0s)
-- Build files have been written to: /home/laolang/gitlab/graver2016/lens_r/startup_project/build/ninja-release
[8/8] Linking C executable dist/bin/graver
2024-02-18 06:08:18.124 123471 INFO [main.c:23] - graver major version is:1
2024-02-18 06:08:18.124 123471 INFO [main.c:24] - graver minor version is:0
2024-02-18 06:08:18.124 123471 INFO [main.c:25] - graver patch version is:0
(.venv) 06:08:18 laolang:startup_project(main)$ ./cm.py rr
rerun_release
clean_release
build_release
cmake --preset=ninja-release
Preset CMake variables:

  CMAKE_BUILD_TYPE="Release"
  CMAKE_C_COMPILER="/usr/bin/clang"
  CMAKE_C_FLAGS="-Wall -Wextra"

-- The C compiler identification is Clang 14.0.0
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working C compiler: /usr/bin/clang - skipped
-- Detecting C compile features
-- Detecting C compile features - done
-- ENABLE_COVERAGE:OFF
-- Configuring done (0.1s)
-- Generating done (0.0s)
-- Build files have been written to: /home/laolang/gitlab/graver2016/lens_r/startup_project/build/ninja-release
cmake --build --preset=ninja-release
[1/8] Building C object src/configuration/CMakeFiles/configuration.dir/version.c.o
[2/8] Building C object src/main/CMakeFiles/graver.dir/main.c.o
[3/8] Building C object test/CMakeFiles/graver_test.dir/test_common.c.o
[4/8] Building C object test/CMakeFiles/graver_test.dir/testmain.c.o
[5/8] Linking C shared library dist/lib/libconfiguration.so.1.0.0
[6/8] Creating library symlink dist/lib/libconfiguration.so.1 dist/lib/libconfiguration.so
[7/8] Linking C executable dist/test/graver_test
[8/8] Linking C executable dist/bin/graver
run_release
/home/laolang/gitlab/graver2016/lens_r/startup_project/build/ninja-release/dist/bin/graver
2024-02-18 06:08:22.216 123539 INFO [main.c:23] - graver major version is:1
2024-02-18 06:08:22.217 123539 INFO [main.c:24] - graver minor version is:0
2024-02-18 06:08:22.217 123539 INFO [main.c:25] - graver patch version is:0

(.venv) 06:08:22 laolang:startup_project(main)$ 
```

### 使用过程截图

![](/assets/image/setup/a01_vscode/004.png)

### 更新配置
所有的配置都在 `.vsocde` 目录,可根据需要自行修改

## 参考资料
[VScode配置C++开发工具链(借助clangd+cmake)](https://zhuanlan.zhihu.com/p/603687041)

[[VSCode] Todo Tree VSCode插件 待办事项树](https://www.cnblogs.com/donpangpang/p/14612568.html)
